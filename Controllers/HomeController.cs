﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webapp.Models;

namespace webapp.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db;

        public ActionResult Index()
        {
            if (this.db == null)
            {
               this.db = ApplicationDbContext.Create();
            }

            if (Request.IsAuthenticated)
            {
                List<Tasks> TasksList = this.db.Task.ToList();
                List<Tasks> NiceList, TheresTimeList, DangerList, LateList;

                NiceList = new List<Tasks>();
                TheresTimeList = new List<Tasks>();
                DangerList = new List<Tasks>();
                LateList = new List<Tasks>();

                foreach (Tasks item in TasksList)
                {
                    if (item.Classification == 0)
                    {
                        NiceList.Add(item);
                    }
                    else if (item.Classification == 1)
                    {
                        TheresTimeList.Add(item);
                    }
                    else if (item.Classification == 2)
                    {
                        DangerList.Add(item);
                    }
                    else
                    {
                        LateList.Add(item);
                    }
                }

                TasksListModel TaskList = new TasksListModel();
                TaskList.NiceList = NiceList;
                TaskList.TheresTimeList = TheresTimeList;
                TaskList.DangerList = DangerList;
                TaskList.LateList = LateList;

                return View(TaskList);
            }
            return RedirectToAction("Login", "Account");
            
        }

        public ActionResult About()
        {
            ViewBag.Title = "Sobre";
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [HttpGet]
        public ActionResult Learning()
        {
            return View();
        }
    }
}