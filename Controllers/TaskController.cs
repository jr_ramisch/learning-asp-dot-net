﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webapp.Models;

namespace webapp.Controllers
{
    public class TaskController : Controller
    {
        private readonly ApplicationDbContext _db;

        private static SelectListItem[] DropDownOptions = new[]{
                new SelectListItem(){ Value = "0", Text = "Nice"},
                new SelectListItem(){ Value = "1", Text = "There's time"},
                new SelectListItem(){ Value = "2", Text = "Danger"},
                new SelectListItem(){ Value = "3", Text = "Late"}
         };

        public TaskController()
        {
            this._db = ApplicationDbContext.Create();
        }

        // GET: Task
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (Request.IsAuthenticated)
            {
                if (TempData["TaskValidation"] != null)
                {
                    ViewBag.Error = TempData["TaskValidation"].ToString();
                }

                ViewBag.PageName = "Criar nova tarefa";
                ViewData["Classification"] = DropDownOptions;

                return View();
            }

            return RedirectToAction("Login", "Account");
        }

        public ActionResult Store(Tasks model)
        {
            if (ModelState.IsValid)
            {
                this._db.Task.Add(model);
                this._db.SaveChanges();

                return RedirectToAction("Index", "Home");
            }

            else
            {
                TempData["TaskValidation"] = "Seria massa se você preenchesse TODOS os campos!";
            }

            return RedirectToAction("Create");
        }
        
        [HttpGet]
        public ActionResult Update(int id)
        {
            Tasks Task = this._db.Task.Find(id);

            ViewData["Date"] = Task.DeadLine.ToString("yyyy-MM-dd").Substring(0, 10);
            ViewData["Classification"] = DropDownOptions;
            ViewBag.PageName = "Modificar tarefa";

            return View(Task);
        }

        [HttpPost]
        public ActionResult Update(Tasks Task)
        {
            Tasks DBTask = this._db.Task.Find(Task.Id);
            DBTask.Name = Task.Name;
            DBTask.Description = Task.Description;
            DBTask.DeadLine = Task.DeadLine;
            DBTask.Classification = Task.Classification;
            this._db.SaveChanges();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Delete(int id)
        {
            Tasks Tasks = this._db.Task.Find(id);
            this._db.Task.Remove(Tasks);
            this._db.SaveChanges();

            return RedirectToAction("Index", "Home");
        }
    }
}