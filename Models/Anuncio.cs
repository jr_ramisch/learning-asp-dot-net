﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webapp.Models
{
    public class Anuncio
    {
        [Key]
        public int AnuncioId { set; get; }
        public string Nome { set; get; }
        public string Descricao { set; get; }
        public DateTime DataInicio { set; get; }
        public DateTime DataFim { set; get; }

        [MaxLength]
        public string Imagem { set; get; }
        public string CaminhoImagem { set; get; }
        public string Link { set; get; } 

    }
}