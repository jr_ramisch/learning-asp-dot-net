﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webapp.Models
{
    public class Produto
    {
        [Key]
        public int ProdutoId { set; get; }
        public string Nome { set; get; }
        public string Descricao { set; get; }
        public bool Ativado { set; get; }
        public DateTime DataCriacao { set; get; }
        public DateTime DataExclusao { set; get; }
        public int IdUser { set; get; }

    }
}