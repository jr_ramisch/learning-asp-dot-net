﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webapp.Models
{
    public class TasksListModel
    {
        public List<Tasks> NiceList { set; get; }
        public List<Tasks> TheresTimeList { set; get; }
        public List<Tasks> DangerList { set; get; }
        public List<Tasks> LateList { set; get; }

    }
}