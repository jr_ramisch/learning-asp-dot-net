﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webapp.Models
{
    public class Tasks
    {

        public int Id { set; get; }

        [Required]
        [Display(Name = "Nome")]
        public string Name { set; get; }

        [Required]
        [Display(Name = "Descrição")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "DeadLine")]
        public DateTime DeadLine { get; set; }

        [Required]
        [Display(Name = "Classisficação")]
        public int Classification { get; set; }
    }
}