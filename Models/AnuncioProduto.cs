﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace webapp.Models
{
    public class AnuncioProduto
    {
        public int Id { get; set; }

        public int ProdutoId { get; set; }
        public int AnuncioId { get; set; }

        [ForeignKey("ProdutoId")]
        public virtual Produto Produto { get; set; }

        [ForeignKey("AnuncioId")]
        public virtual Anuncio Anuncio { get; set; }
    }
}