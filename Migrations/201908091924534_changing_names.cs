namespace webapp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changing_names : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Produtoes", newName: "Produto");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Produtos", newName: "Produto");
        }
    }
}
