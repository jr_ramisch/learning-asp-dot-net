namespace webapp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_classification_field_type : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Tasks", "Classification", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tasks", "Classification", c => c.String(nullable: false));
        }
    }
}
