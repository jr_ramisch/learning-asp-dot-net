namespace webapp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creating_raffinads_tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Anuncios",
                c => new
                    {
                        AnuncioId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Descricao = c.String(),
                        DataInicio = c.DateTime(nullable: false),
                        DataFim = c.DateTime(nullable: false),
                        Image = c.String(),
                        CaminhoImagem = c.String(),
                        Link = c.String(),
                    })
                .PrimaryKey(t => t.AnuncioId);
            
            CreateTable(
                "dbo.AnuncioProdutos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProdutoId = c.Int(nullable: false),
                        AnuncioId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Anuncios", t => t.AnuncioId, cascadeDelete: true)
                .ForeignKey("dbo.Produtos", t => t.ProdutoId, cascadeDelete: true)
                .Index(t => t.ProdutoId)
                .Index(t => t.AnuncioId);
            
            CreateTable(
                "dbo.Produtos",
                c => new
                    {
                        ProdutoId = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Descricao = c.String(),
                        Ativado = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false),
                        DataExclusao = c.DateTime(nullable: false),
                        IdUser = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProdutoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnuncioProdutos", "ProdutoId", "dbo.Produtos");
            DropForeignKey("dbo.AnuncioProdutos", "AnuncioId", "dbo.Anuncios");
            DropIndex("dbo.AnuncioProdutos", new[] { "AnuncioId" });
            DropIndex("dbo.AnuncioProdutos", new[] { "ProdutoId" });
            DropTable("dbo.Produtos");
            DropTable("dbo.AnuncioProdutos");
            DropTable("dbo.Anuncios");
        }
    }
}
