namespace webapp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_date_field_type_2_datetime_instead_string : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Tasks", "DeadLine", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tasks", "DeadLine", c => c.String(nullable: false));
        }
    }
}
