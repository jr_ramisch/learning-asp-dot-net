namespace webapp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creating_classification_field : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tasks", "Classification", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tasks", "Classification");
        }
    }
}
