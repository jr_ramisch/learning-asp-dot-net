namespace webapp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class change_context : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Tasks");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        DeadLine = c.DateTime(nullable: false),
                        Classification = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
