namespace webapp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changing_names_again : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Produtos", newName: "Produtoes");
            AddColumn("dbo.Anuncios", "Imagem", c => c.String());
            DropColumn("dbo.Anuncios", "Image");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Anuncios", "Image", c => c.String());
            DropColumn("dbo.Anuncios", "Imagem");
            RenameTable(name: "dbo.Produtoes", newName: "Produtos");
        }
    }
}
