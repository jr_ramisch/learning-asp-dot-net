namespace webapp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Tasks", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Tasks", "Description", c => c.String(nullable: false));
            AlterColumn("dbo.Tasks", "DeadLine", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tasks", "DeadLine", c => c.String());
            AlterColumn("dbo.Tasks", "Description", c => c.String());
            AlterColumn("dbo.Tasks", "Name", c => c.String());
        }
    }
}
